﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Knight : MonoBehaviour {

    private Animator anim;
    [SerializeField] private Transform[] groundPoints;
    private Rigidbody2D myRigidbody;
    [SerializeField] private float groundRadius;
    [SerializeField] private LayerMask whatIsGround;
    private bool isGrounded;
    private bool jump;
    [SerializeField] float jumpForce;
    [SerializeField] public static int health = 6;
    public int healthCheck;
    public bool blocking;
    public Image[] hearts;
    public Transform spawnPoint;
    AudioSource coinAudio;
    AudioSource hurtAudio;
    AudioSource deathAudio;
    public AudioSource blockAudio;

    void Start () {
        anim = GetComponent<Animator>();
        myRigidbody = GetComponent<Rigidbody2D>();
        AudioSource[] audios = GetComponents<AudioSource>();
        coinAudio = audios[2];
        hurtAudio = audios[5];
        deathAudio = audios[4];
        blockAudio = audios[3]; 
	}
	
	void Update () {

        healthCheck = health;

        isGrounded = IsGrounded(); 

        Movement();
        {
            float move = Input.GetAxis("Horizontal");
            anim.SetFloat("Speed", move);
        }

        CheckBlocking();

        for (int i = 0; i < hearts.Length; i++)
        {

            if (i < health)
            {
                hearts[i].enabled = true;
            }
            else
            {
                hearts[i].enabled = false;
            }
        }
    }

    public void CheckBlocking()
    {
        if (Input.GetKey(KeyCode.K))
        {
            blocking = true;
            anim.SetBool("Blocking", true);
        }
        else
        {
            blocking = false;
            anim.SetBool("Blocking", false);
        }


    }

    void Movement()
    {
        if (blocking == false && health > 0)
        {
            if (Input.GetKey(KeyCode.D))
            {
                transform.Translate(Vector2.right * 3f * Time.deltaTime);
                transform.eulerAngles = new Vector2(0, 0);
            }

            if (Input.GetKey(KeyCode.A))
            {
                transform.Translate(-Vector2.right * 3f * Time.deltaTime);
                transform.eulerAngles = new Vector2(0, 0);
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {

                jump = true;

                if (isGrounded && jump)
                {
                    isGrounded = false;

                    myRigidbody.AddForce(new Vector2(0, jumpForce));
                }
            }
        }
    }

    private bool IsGrounded()
    {
        if(myRigidbody.velocity.y <= 0)
        {
            foreach(Transform point in groundPoints)
            {
                Collider2D[] colliders = Physics2D.OverlapCircleAll(point.position, groundRadius, whatIsGround);

                for(int i = 0; i < colliders.Length; i++)
                {
                    if(colliders[i].gameObject != gameObject)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            if (blocking == false)
            {
                TakeDamage(collision.gameObject.GetComponent<Enemy>().damage);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
            if (collision.gameObject.tag == "Coin")
            {
                coinAudio.Play();
            }
    }



    public void TakeDamage(int damage)
    {
        Debug.Log("Took" + damage + " damage.");
        health -= damage;
        hurtAudio.Play();

        if(health <= 0)
        {
            Die();
        }
    }

    public bool Die()
    {
        Score.playerScore -= 500;
        anim.SetTrigger("Die");
        deathAudio.Play();

        gameObject.transform.position = spawnPoint.position;
        health = 6;
        return true;
    }
}
