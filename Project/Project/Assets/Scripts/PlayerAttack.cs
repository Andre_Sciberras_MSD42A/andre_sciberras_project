﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour {

    private Animator anim;
    private float timeBtwAttack;
    public float startTimeBtwAttack;
    public Transform attackPos;
    public float attackRange;
    public LayerMask whatIsEnemies;
    public int damage;
    AudioSource splash;
    AudioSource swoosh;


    void Start()
    {
        anim = GetComponent<Animator>();
        AudioSource[] audios = GetComponents<AudioSource>();
        splash = audios[0];
        swoosh = audios[1];
    }

    private void Update()
    {
        if (timeBtwAttack <= 0 && GetComponent<Knight>().blocking == false)
        {
            if (Input.GetKey(KeyCode.J))
            {
                anim.SetTrigger("Attack");
                swoosh.Play();
                timeBtwAttack = startTimeBtwAttack;
                Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatIsEnemies);
                for (int i = 0; i < enemiesToDamage.Length; i++)
                {
                    enemiesToDamage[i].GetComponent<Enemy>().TakeDamage(damage);
                    splash.Play();
                }
            }
        }
        else
        {
            timeBtwAttack -= Time.deltaTime;
        }
     }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position, attackRange);
    }

}
