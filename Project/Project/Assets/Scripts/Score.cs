﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    public static int playerScore = 0;
    public Text scoreTXT;

    public void IncreaseScore(int scoreToIncrease)
    {
        playerScore += scoreToIncrease;
    }

    private void Update()
    {
        scoreTXT.text = playerScore.ToString();
    }

}
