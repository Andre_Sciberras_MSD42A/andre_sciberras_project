﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallCollider : MonoBehaviour {

    public Transform respawnPoint;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player" && collision.GetComponent<Knight>().healthCheck == 0)
        {
            collision.GetComponent<Knight>().Die();
        }
        else
        {
            collision.transform.position = respawnPoint.position;
            collision.GetComponent<Knight>().TakeDamage(1);
        }
    }
}
