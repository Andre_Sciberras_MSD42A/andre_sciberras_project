﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedEnemy : MonoBehaviour {

    public Transform firePoint;
    public Transform range;
    [SerializeField] GameObject projectile;
    [SerializeField] float attackSpeed;
    [SerializeField] float attackRange;
    private float timeBtwAttack;
    private Vector2 target;
    private RaycastHit2D playerDetection;
    private Animator anim;
    AudioSource shootAudio;

    private void Start()
    {
        AudioSource[] audios = GetComponents<AudioSource>();
        anim = GetComponent<Animator>();
        shootAudio = audios[1];
    }

    void Update () {

        playerDetection = Physics2D.Raycast(range.position, new Vector2(attackRange, attackRange), attackRange);

        
        if (playerDetection.collider == true && playerDetection.collider.gameObject.tag == "Player")
        {

            Debug.Log("Player Detected");

            this.GetComponent<EnemyPatrol>().actualSpeed = 0;

            if (timeBtwAttack <= 0)
            {
                anim.SetTrigger("Attack");
                Instantiate(projectile, firePoint.position, Quaternion.identity);
                shootAudio.Play();
                timeBtwAttack = attackSpeed;
            }

        }
            timeBtwAttack -= Time.deltaTime;
    }

   

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(range.position, new Vector2(attackRange, 1f));
    }

}
