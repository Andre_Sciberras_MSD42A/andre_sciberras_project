﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextManager : MonoBehaviour {

    [SerializeField] Text tutorialText;
    [SerializeField] string newText;

    private void OnTriggerEnter2D()
    {
        tutorialText.text = newText;
    }
}
