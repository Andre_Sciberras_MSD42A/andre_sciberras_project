﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    // Use this for initialization
    public void LoadLevel(string name)
    {
        Debug.Log("Loading level " + name);
        SceneManager.LoadScene(name);
    }


    //To close the game when it is an applicaton (.exe)
    public void QuitGame()
    {
        Application.Quit();
    }
}
