﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    private Transform playerTransform;

    private void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void LateUpdate()
    {
        //Storing current camera's position
        Vector3 temp = transform.position;

        //Setting camera position equal to player position
        temp.x = playerTransform.position.x;

        //Setting back camera's temp position to camera's current position
        transform.position = temp;
    }
}
