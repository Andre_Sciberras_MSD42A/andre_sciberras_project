﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    [SerializeField]public int health;
    private float dazedTime;
    public float startDazedTime;
    public GameObject blood;
    [SerializeField]public int damage;

    void Update()
    {
        if (dazedTime <= 0) {
            GetComponent<EnemyPatrol>().actualSpeed = GetComponent<EnemyPatrol>().preSetSpeed;
        }
        else
        {
            GetComponent<EnemyPatrol>().actualSpeed = 0;
            dazedTime -= Time.deltaTime;
        }
    }

    public void TakeDamage(int damage)
    {
        GetComponent<AudioSource>().Play();
        Instantiate(blood, transform.position, Quaternion.identity);
        dazedTime = startDazedTime;
        health -= damage;
        if(health <= 0)
        {
            Score.playerScore += 200;
            Destroy(gameObject);
        }
    }
}
